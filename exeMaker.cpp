#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include <cstdlib>
using namespace std;
int main(){
    ofstream shFile("makeExe.sh");
    bool mac = false;
    string isMac;
    cout << "are you on a mac?[yes/no] ";
    getline(cin, isMac);
    if(isMac == "yes")
        mac = true;
    if(isMac == "no")
        mac = false;
    cout << "file path: ";
    string path;
    string name;
    getline(cin, path);
    cout << "name of exe: ";
    getline(cin, name);
    cout << "getting dependencies...\n";
    if(mac == false)
        system("sudo apt install mingw-w64");
    system("cd\n");
    if(mac == false){
        shFile << "x86_64-w64-mingw32-gcc -o " + name + " " + path + " -lstdc++ -lm -static";
    } else {
        shFile << "gcc -o " + name + " " + path + " -lstdc++ -lm -static";
    }
    shFile.close();
    system("bash ./makeExe.sh");
    cout << "your exe should be in the dpnd folder\n";
}
